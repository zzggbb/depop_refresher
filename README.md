# Depop Python API and Depop Management Application

This project is hosted online at `https://gitlab.com/zzggbb/depop_manager`

## Installation
You must install dependencies before use:

```bash
$ pip3 install -r requirements.txt
```

## Usage of Management Application

```bash
$ python3 depop_manager.py
```

Then follow the prompts to use the script. The script can:

### Refresh
* Refresh a single item
* Refresh items in a range between two items
* Refresh all unsold items

### Relist
* Relist a single item, and optionally delete the original listing
* Relist items in a range between two items, and optionally delete the original listings

### Other
* Print a list of all unsold items
