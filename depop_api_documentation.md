# Bearer Token Authorization Levels
* Level 0: No authorization header is required to make the request
* Level 1: An authorization header of the form `Authorization: Bearer <token>` is required.
  But the request can be made towards objects that are not necessarily owned by the user
  associated with the token.
* Level 1.5: An authorization header of the form `Authorization: Bearer <token>` is required.
  Any user can make the request, but the request will create an object that can later only
  be manipulated by the user associated with the token that created it.
* Level 2: An authorization header of the form `Authorization: Bearer <token>` is required.
  The token must be associated with the user who owns the object.

## Get User Information
* level 0 authorization
* `GET https://webapi.depop.com/api/v1/shop/{username}`
* no request data
* success code: 200
* response data:
```
  {
    'username': 'slamdunkvintage206',
    'verified': True,
    'bio': 'Fire vintage and designer from Seattle WA! All items shipped in under 2 business days!! DM FOR BUNDLES 🔥🔥',
    'initials': 'S\ud83c',
    'id': 6850252,
    'picture': {
      '75': 'https://pictures.depop.com/b0/6850252/824314718_eea2620a808e42e9be4506185ea65939/U2.jpg',
      '113': 'https://pictures.depop.com/b0/6850252/824314718_eea2620a808e42e9be4506185ea65939/U3.jpg',
      '150': 'https://pictures.depop.com/b0/6850252/824314718_eea2620a808e42e9be4506185ea65939/U1.jpg',
      '225': 'https://pictures.depop.com/b0/6850252/824314718_eea2620a808e42e9be4506185ea65939/U4.jpg',
      '300': 'https://pictures.depop.com/b0/6850252/824314718_eea2620a808e42e9be4506185ea65939/U5.jpg'
    },
    'website': '',
    'first_name': 'Slam Dunk Vintage',
    'last_name': '🏀🏀⛹🏽⛹️\u200d♀️',
    'followers': 5397,
    'following': 717,
    'reviews_total': 436,
    'reviews_rating': 5,
    'last_seen': '2021-12-14T22:43:23.093068Z',
    'items_sold': 1179
  }
```

## Get Item Information
* level 2 authorization
* `GET https://webapi.depop.com/api/v2/products/{item_slug}`
* no request data
* success code: 200
* response data:
```
    {
      'description': 'test5',
      'id': 273796830,
      'variantSetId': 60,
      'variants': {'4': 1},
      'slug': 'hifiboombox-test5',
      'address': 'Seattle, United States of America',
      'countryCode': 'US',
      'categoryId': 35,
      'pictures': [
        [
          {'id': 1142358043, 'width': 320, 'height': 320, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P5.jpg'},
          {'id': 1142358043, 'width': 150, 'height': 150, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P2.jpg'},
          {'id': 1142358043, 'width': 1280, 'height': 1280, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P0.jpg'},
          {'id': 1142358043, 'width': 1280, 'height': 1280, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P8.jpg'},
          {'id': 1142358043, 'width': 640, 'height': 640, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P1.jpg'},
          {'id': 1142358043, 'width': 960, 'height': 960, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P7.jpg'},
          {'id': 1142358043, 'width': 210, 'height': 210, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P4.jpg'},
          {'id': 1142358043, 'width': 480, 'height': 480, 'url': 'https://media-photos.depop.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P6.jpg'}
        ]
      ],
      'shippingMethods': [],
      'brandId': 5,
      'status': 'ONSALE',
      'condition': 'brand_new',
      'colour': ['brown'],
      'age': ['60s'],
      'source': ['preloved'],
      'style': ['goth'],
      'price': {
        'priceAmount': '50.00',
        'currencyName': 'USD',
        'nationalShippingCost': '5.00',
        'internationalShippingCost': '1.00'
      },
      'brand': 'giuseppe-zanotti',
      'group': 'bottoms',
      'attributes': {
        'body-fit': ['petite'],
        'bottom-fit': ['capris'],
        'material': ['canvas', 'corduroy'],
        'occasion': ['outdoors', 'prom', 'wedding'],
        'bottom-style': ['flare', 'low-rise']
      },
      'productType': 'jeans',
      'gender': 'male',
      'isKids': False
    }
```

## Delete Item
* level 2 authorization
* `DELETE https://webapi.depop.com/api/v1/products/{item_id}`
* no request data
* success code: 204
* no response data

## Upload Item Image (step 1)
* level 1.5 authorization
* POST https://webapi.depop.com/api/v2/pictures/
* request data:
```
  {
    type: "product",
    extension: "jpg"
  }
```
* success code: 201
* response data:
```
  {
    "id":1142358043,
    "url":"https://us-east-1-prod-app-service-media-default-picture-api-page.s3.amazonaws.com/b0/33468854/1142358043_887010c633dc4de0ba9425aec3d02e33/P0.jpg?AWSAccessKeyId=ASIA47JNX4DMW5OEEW2K&Signature=N9Z7F9tPmZQc%2Fe8tSUfG%2BfxsbMk%3D&content-type=image%2Fjpeg&x-amz-security-token=FwoGZXIvYXdzEJL%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDJOSnxpYvKpCQwZD0yKsAsKw%2F7kmc1iNlVFu80KGfn5JDrU4A6m8dPdaLkSoEWdkgyoCGeHxzDJRCMCt6B4s4joPBMdFkOuzc8c7uLyctLQb%2FyxInLwfhoKnINml8G2Rb%2Fk87cicUQC3vgzcU1XA0kwSVL6mqYdVJVt%2BurD8VggWiAUe%2B%2BnHyZZ%2F6iviu2xESqiZy1lfx5CRR9mifyJ5J6JlCvKH6Y%2B6sdSPuCyfBVlt4ne0ebLUjP3kW6A3n%2FO9%2FN90lqGbmZO0ex5orfqhWjqrT4etgrYvcTNMOmzoF0fbGXUQqmwROyy81h15lXSPR9E845eThMf7ZFt1CsOw3%2BdzA8Unl2NMyl49k3xawz9MPeHTRChTZ%2F%2F0g8vEfITFcsIxljF5NFR6WgYCPCfhDpbRPIZ7oLFI4J2MySjs8%2BSNBjIt4he67bU4V9OPd4cIKAZVwxjYMD6owDGWlqOxzzt8wuzBeuROf4vVSPxrfXUv&Expires=1639533625",
    "content_type":"image/jpeg",
    "expires_at":"2021-12-15T02:00:25.463891Z"
  }
```

## Upload Item Image (step 2)
* level 0 authorization
* `PUT {url obtained from step 1}`
* Must include header `Content-Type: image/jpeg`
* request data: <bytes of image>
* success code: 200
* no response data

## Post Item
* level 1.5 authorization
* `POST https://webapi.depop.com/api/v2/products/`
* request data:
```
  {
    "pictureIds":[1142358043],
    "description":"test5",
    "group":"bottoms",
    "productType":"jeans",
    "attributes":{
      "bottom-fit":["capris"],
      "bottom-style":["flare","low-rise"],
      "occasion":["outdoors","prom","wedding"],
      "material":["canvas","corduroy"],
      "body-fit":["petite"]
    },
    "gender":"male",
    "variantSetId":60,
    "nationalShippingCost":"5.00",
    "internationalShippingCost":"1.00",
    "priceAmount":"50",
    "variants":{
      "4":1
    },
    "brand":"giuseppe-zanotti",
    "condition":"brand_new",
    "colour":["brown"],
    "source":["preloved"],
    "age":["60s"],
    "style":["goth"],
    "shippingMethods":[],
    "priceCurrency":"USD",
    "geoLat":47.6038,
    "geoLng":-122.33,
    "address":"Seattle, United States of America",
    "countryCode":"US",
    "isKids":false
  }
```
* success code: 201
* response data:
```
  {
    "id":273796830,
    "slug":"hifiboombox-test5"
  }
```
