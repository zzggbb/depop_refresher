# standard imports
import time
import logging

# local imports
import depop_api

# how long to wait in between refreshing items, in seconds
REFRESH_INTERVAL = 5

# how long to wait in between relisting items, in seconds
RELIST_INTERVAL = 5

AUTH_TOKEN_PROMPT = """
Please provide a depop authorization token. To obtain this token:
  1. Open chrome://settings/cookies/detail?site=depop.com
  2. Expand the section labeled "access_token"
  3. Copy the 40-character key within the "Content" section
Please paste the 40 character key and then press enter:
> """

BAD_AUTH_ERROR = """
The depop authorization token you provided is either invalid, or
you do not have permission to control the specified user's listings.
Please double check it and then run this program again.
"""

USERNAME_PROMPT = """
Please provide a depop username:
> """

BAD_USERNAME_ERROR = """
The depop username you provided is invalid.
Please double check it and then run this program again.
"""

MODE_PROMPT = """
What would you like to do? Press one of the following keys:
q - quit
fs - refresh a single item
fr - refresh items in a range between two items
fa - refresh all unsold items
ls - relist a single item, without deleting the original listing
lr - relist items in a range between two items, without deleting original listings
lsd - relist a single item, and delete the original listing
lrd - relist items in a range between two items, and delete the original listings
p - print list of all unsold items
> """

def prompt_list_item(data, prompt):
  while True:
    try:
      index = data.index(input(prompt + ': '))
      break
    except ValueError:
      print("invalid item name, try again")
      continue

  return index

def main():
  username = input(USERNAME_PROMPT)
  user_id = depop_api.DepopAPI.get_user_id(username)
  if user_id is None:
    print(BAD_USERNAME_ERROR)
    return

  auth_token = input(AUTH_TOKEN_PROMPT)
  api_instance = depop_api.DepopAPI(auth_token)

  try:
    unsold_items = api_instance.get_unsold_items(user_id)
  except depop_api.BadAuthorizationException:
    print(BAD_AUTH_ERROR)
    return

  while True:
    mode = input(MODE_PROMPT).lower()
    if mode == 'q':
      break

    elif mode == '':
      continue

    elif mode == 'fs':
      index_a = prompt_list_item(unsold_items, 'item name')
      index_b = index_a + 1
      operation = 'refresh'

    elif mode == 'fr':
      index_a = prompt_list_item(unsold_items, 'first item name')
      index_b = prompt_list_item(unsold_items, 'last item name') + 1
      operation = 'refresh'

    elif mode == 'fa':
      index_a = 0
      index_b = len(unsold_items)
      operation = 'refresh'

    elif mode == 'ls':
      index_a = prompt_list_item(unsold_items, 'item name')
      index_b = index_a + 1
      operation = 'relist'
      delete_old = False

    elif mode == 'lr':
      index_a = prompt_list_item(unsold_items, 'first item name')
      index_b = prompt_list_item(unsold_items, 'last item name') + 1
      operation = 'relist'
      delete_old = False

    elif mode == 'lsd':
      index_a = prompt_list_item(unsold_items, 'item name')
      index_b = index_a + 1
      operation = 'relist'
      delete_old = True

    elif mode == 'lrd':
      index_a = prompt_list_item(unsold_items, 'first item name')
      index_b = prompt_list_item(unsold_items, 'last item name') + 1
      operation = 'relist'
      delete_old = True

    elif mode == 'p':
      for i, name in enumerate(unsold_items):
        print(f"item #{i+1: <5}: {name}")
      continue

    else:
        print(f"unrecognized mode '{mode}', try again")
        continue

    # freeze list of unsold items while we are operating on them
    item_slugs = [unsold_items[i] for i in range(index_a, index_b)[::-1]]
    for item_slug in item_slugs:
      if operation == 'refresh':
        api_instance.refresh_item(item_slug)
        unsold_items.remove(item_slug)
        unsold_items.insert(0, item_slug)
        time.sleep(REFRESH_INTERVAL)

      elif operation == 'relist':
        new_slug = api_instance.relist_item(item_slug, delete_old=delete_old)
        if delete_old:
          unsold_items.remove(item_slug)
        unsold_items.insert(0, new_slug)
        time.sleep(RELIST_INTERVAL)

      else:
        pass


if __name__ == '__main__':
  main()
